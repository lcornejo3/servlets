<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 31/10/2020
  Time: 13:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Login</title>
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    td{
      padding: 10px
    }
    div{
      width: 50%;
      border: 1px solid black;
      border-radius: 5px;
      background-color: lightgrey;
    }
  </style>
</head>
<body>
<h1><u>Login Here</u></h1>
<div>
  <form action="Servlet" method="POST">
    <table>
      <tr>
        <td>User name</td>
        <td><input type="text" class="form-control" name="username" placeholder="User name"></td>
      </tr>
      <tr>
        <td>Password</td>
        <td><input type="password" class="form-control" name="password" placeholder="Password"></td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center"><input class="btn-success" type="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
</div>
</body>
</html>
